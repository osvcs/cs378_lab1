// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef CS378_LAB1_CS378_Lab1Projectile_generated_h
#error "CS378_Lab1Projectile.generated.h already included, missing '#pragma once' in CS378_Lab1Projectile.h"
#endif
#define CS378_LAB1_CS378_Lab1Projectile_generated_h

#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_SPARSE_DATA
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACS378_Lab1Projectile(); \
	friend struct Z_Construct_UClass_ACS378_Lab1Projectile_Statics; \
public: \
	DECLARE_CLASS(ACS378_Lab1Projectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab1"), NO_API) \
	DECLARE_SERIALIZER(ACS378_Lab1Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesACS378_Lab1Projectile(); \
	friend struct Z_Construct_UClass_ACS378_Lab1Projectile_Statics; \
public: \
	DECLARE_CLASS(ACS378_Lab1Projectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab1"), NO_API) \
	DECLARE_SERIALIZER(ACS378_Lab1Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACS378_Lab1Projectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACS378_Lab1Projectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACS378_Lab1Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Lab1Projectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACS378_Lab1Projectile(ACS378_Lab1Projectile&&); \
	NO_API ACS378_Lab1Projectile(const ACS378_Lab1Projectile&); \
public:


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACS378_Lab1Projectile(ACS378_Lab1Projectile&&); \
	NO_API ACS378_Lab1Projectile(const ACS378_Lab1Projectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACS378_Lab1Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Lab1Projectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACS378_Lab1Projectile)


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ProjectileMesh() { return STRUCT_OFFSET(ACS378_Lab1Projectile, ProjectileMesh); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ACS378_Lab1Projectile, ProjectileMovement); }


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_12_PROLOG
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_SPARSE_DATA \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_RPC_WRAPPERS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_INCLASS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_SPARSE_DATA \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_INCLASS_NO_PURE_DECLS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_LAB1_API UClass* StaticClass<class ACS378_Lab1Projectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Projectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
