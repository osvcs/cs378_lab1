// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CS378_LAB1_CS378_Lab1GameMode_generated_h
#error "CS378_Lab1GameMode.generated.h already included, missing '#pragma once' in CS378_Lab1GameMode.h"
#endif
#define CS378_LAB1_CS378_Lab1GameMode_generated_h

#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_SPARSE_DATA
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_RPC_WRAPPERS
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACS378_Lab1GameMode(); \
	friend struct Z_Construct_UClass_ACS378_Lab1GameMode_Statics; \
public: \
	DECLARE_CLASS(ACS378_Lab1GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab1"), CS378_LAB1_API) \
	DECLARE_SERIALIZER(ACS378_Lab1GameMode)


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACS378_Lab1GameMode(); \
	friend struct Z_Construct_UClass_ACS378_Lab1GameMode_Statics; \
public: \
	DECLARE_CLASS(ACS378_Lab1GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab1"), CS378_LAB1_API) \
	DECLARE_SERIALIZER(ACS378_Lab1GameMode)


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CS378_LAB1_API ACS378_Lab1GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACS378_Lab1GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CS378_LAB1_API, ACS378_Lab1GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Lab1GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CS378_LAB1_API ACS378_Lab1GameMode(ACS378_Lab1GameMode&&); \
	CS378_LAB1_API ACS378_Lab1GameMode(const ACS378_Lab1GameMode&); \
public:


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CS378_LAB1_API ACS378_Lab1GameMode(ACS378_Lab1GameMode&&); \
	CS378_LAB1_API ACS378_Lab1GameMode(const ACS378_Lab1GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CS378_LAB1_API, ACS378_Lab1GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Lab1GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACS378_Lab1GameMode)


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_9_PROLOG
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_SPARSE_DATA \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_RPC_WRAPPERS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_INCLASS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_SPARSE_DATA \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_INCLASS_NO_PURE_DECLS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_LAB1_API UClass* StaticClass<class ACS378_Lab1GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CS378_Lab1_Source_CS378_Lab1_CS378_Lab1GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
