// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CS378_LAB1_CS378_Lab1Pawn_generated_h
#error "CS378_Lab1Pawn.generated.h already included, missing '#pragma once' in CS378_Lab1Pawn.h"
#endif
#define CS378_LAB1_CS378_Lab1Pawn_generated_h

#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_SPARSE_DATA
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnMyDashPressed); \
	DECLARE_FUNCTION(execOnMyFirePressed);


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnMyDashPressed); \
	DECLARE_FUNCTION(execOnMyFirePressed);


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_EVENT_PARMS
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_CALLBACK_WRAPPERS
#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACS378_Lab1Pawn(); \
	friend struct Z_Construct_UClass_ACS378_Lab1Pawn_Statics; \
public: \
	DECLARE_CLASS(ACS378_Lab1Pawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab1"), NO_API) \
	DECLARE_SERIALIZER(ACS378_Lab1Pawn)


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACS378_Lab1Pawn(); \
	friend struct Z_Construct_UClass_ACS378_Lab1Pawn_Statics; \
public: \
	DECLARE_CLASS(ACS378_Lab1Pawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab1"), NO_API) \
	DECLARE_SERIALIZER(ACS378_Lab1Pawn)


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACS378_Lab1Pawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACS378_Lab1Pawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACS378_Lab1Pawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Lab1Pawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACS378_Lab1Pawn(ACS378_Lab1Pawn&&); \
	NO_API ACS378_Lab1Pawn(const ACS378_Lab1Pawn&); \
public:


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACS378_Lab1Pawn(ACS378_Lab1Pawn&&); \
	NO_API ACS378_Lab1Pawn(const ACS378_Lab1Pawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACS378_Lab1Pawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Lab1Pawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACS378_Lab1Pawn)


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ShipMeshComponent() { return STRUCT_OFFSET(ACS378_Lab1Pawn, ShipMeshComponent); } \
	FORCEINLINE static uint32 __PPO__CameraComponent() { return STRUCT_OFFSET(ACS378_Lab1Pawn, CameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ACS378_Lab1Pawn, CameraBoom); }


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_9_PROLOG \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_EVENT_PARMS


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_SPARSE_DATA \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_RPC_WRAPPERS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_CALLBACK_WRAPPERS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_INCLASS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_SPARSE_DATA \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_CALLBACK_WRAPPERS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_INCLASS_NO_PURE_DECLS \
	CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_LAB1_API UClass* StaticClass<class ACS378_Lab1Pawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CS378_Lab1_Source_CS378_Lab1_CS378_Lab1Pawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
