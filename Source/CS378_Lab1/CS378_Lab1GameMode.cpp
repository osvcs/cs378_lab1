// Copyright Epic Games, Inc. All Rights Reserved.

#include "CS378_Lab1GameMode.h"
#include "CS378_Lab1Pawn.h"

ACS378_Lab1GameMode::ACS378_Lab1GameMode()
{
	// set default pawn class to our character class
	// FOR LAB 1
	static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Class'/Game/TwinStick/Blueprints/BP_CS378_Lab1Pawn.BP_CS378_Lab1Pawn_C'"));

	if (pawnBPClass.Object) {
		if(GEngine){ 
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Found")); 
		}
		UClass* pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else {
		if(GEngine){ 
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Not Found")); 
		}			
		DefaultPawnClass = ACS378_Lab1Pawn::StaticClass();
	}
}

