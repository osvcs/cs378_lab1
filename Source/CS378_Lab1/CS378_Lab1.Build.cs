// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class CS378_Lab1 : ModuleRules
{
	public CS378_Lab1(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
	}
}
